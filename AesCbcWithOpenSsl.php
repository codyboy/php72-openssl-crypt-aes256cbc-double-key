<?php
define('OWNER_KEY', '1234');
define('OWNER_IV_KEY', '4321');
define('ENCRYPT_METHOD', 'AES-256-CBC');


function _Crypt(
    string $_string
) : ?string
{
    try {
        $init_vect = substr(
            hash(
                'sha256',
                OWNER_IV_KEY
            ),
            0,
            openssl_cipher_iv_length(ENCRYPT_METHOD)
        );
        return sprintf(
            "%s:%s",
            $init_vect,
            base64_encode(
                openssl_encrypt(
                    $_string,
                    ENCRYPT_METHOD,
                    hash(
                        'sha256',
                        OWNER_KEY
                    ),
                    0,
                    $init_vect
                )
            )
        );
    } catch ( Exception $e ) {
        die($e);
    }
}


function _Decrypt(
    string $_string
) : ?string
{
    try {
        $parts = explode(
            ':',
            $_string
        );
        return openssl_decrypt(
            base64_decode($parts[1]),
            ENCRYPT_METHOD,
            hash(
                'sha256',
                OWNER_KEY
            ),
            0,
            $parts[0]
        );
    } catch ( Exception $e ) {
        die($e);
    }
}
